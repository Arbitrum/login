package com.iproinfo.index.entity;

import lombok.Data;

/**
 * @Package: com.iproinfo.index.entity
 * @ClassName: User
 * @Author: 86166
 * @CreateTime: 2020/12/30 15:18
 * @Description:
 */
@Data
public class User {
    private String userName;
    private String passWord;
    private String salt;
    private String ca;

    public String getCa() {
        return ca;
    }

    public void setCa(String ca) {
        this.ca = ca;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
}
