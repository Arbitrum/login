package com.iproinfo.index.mapper;

import com.iproinfo.index.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Package: com.iproinfo.index.mapper
 * @ClassName: MenuDao
 * @Author: Zy_Rong
 * @CreateTime: 2020/12/23 14:46
 * @Description:
 */
@Mapper
public interface MenuDao {
    //注册——密码使用PBKDF2进行加密
    int addUser(User user);
    //根据userName查找当时注册的盐值
    User findSalt(@Param("userName") String userName);
    //登录验证
    User findOneUser(@Param("userName") String userName, @Param("passWord") String passWord);
    //登录失败
    int failedUserLogin(@Param("userName") String userName, @Param("time") String time);
    //查询10分钟内登录失败次数
    int logonFailureTimes(@Param("userName") String userName, @Param("newTime") String newTime, @Param("oldTime") String oldTime);
}
